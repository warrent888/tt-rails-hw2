class AlterColumnTaskDue < ActiveRecord::Migration
  def self.up
  	change_table :tasks do |t|
      t.change :due, :integer
    end
  end

  def self.down
  	change_table :tasks do |t|
      t.change :due, :timestamp
    end
  end

end
