class TaskController < ApplicationController
	def index
		@tasks = Task.all
	end

	def edit
		@task = Task.find(params[:id])
	end

	def new
	end

	def update
	  task = Task.find(params[:id])
	  task.update_attributes(task_params)
      redirect_to :root
    end

	def create
		@task = Task.new(task_params)
		if @task.save
			redirect_to :root, notice: 'Your task was successfully posted!'
		else
			redirect_to :back, notice: 'Your task was unsuccessfully posted!'
		end
	end

	def destroy
		task = Task.find(params[:id])
		task.destroy
		redirect_to :root, notice: 'Your task was successfully deleted!'
	end

	private

	def task_params
		params.require(:task).permit(:name, :description, :due)
	end

end
