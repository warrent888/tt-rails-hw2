Rails.application.routes.draw do

  root to: "task#index", via: :get
  resources :task

  post "task/new", :controller => 'task', :action => 'create'
  get "task/:id/edit", :controller => 'task', :action => 'edit'
  post "task/:id/edit", :controller => 'task', :action => 'update'
  post "task/:id/destroy", :controller => 'task', :action => 'delete'


end
